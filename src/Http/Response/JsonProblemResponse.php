<?php declare(strict_types=1);

namespace App\Http\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

use function array_filter;
use function array_merge;

class JsonProblemResponse extends JsonResponse
{
    private const DEFAULT_TYPE = 'about:blank';

    /**
     * @param string      $title
     * @param string      $detail
     * @param int         $status
     * @param string|null $type
     * @param mixed       $data
     * @param string[]    $headers
     */
    public function __construct(
        string $title,
        string $detail,
        int $status = 400,
        ?string $type = null,
        $data = null,
        array $headers = []
    ) {
        $type = $type ?? self::DEFAULT_TYPE;

        parent::__construct($this->format($type, $title, $detail, $data), $status, $this->getHeaders($headers), false);
    }

    /**
     * @param string $type
     * @param string $title
     * @param string $detail
     * @param mixed  $data
     *
     * @return mixed[]
     */
    private function format(string $type, string $title, string $detail, $data): array
    {
        return array_filter(compact('type', 'title', 'detail', 'data'));
    }

    /**
     * @param string[] $headers
     * @return string[]
     */
    private function getHeaders(array $headers): array
    {
        return array_merge($headers, ['Content-Type' => 'application/json+problem']);
    }
}
