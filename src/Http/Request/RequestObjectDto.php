<?php

declare(strict_types=1);

namespace App\Http\Request;

use App\Resolver\Controller\Argument\DeserializableJsonRequestObjectInterface;

abstract class RequestObjectDto implements DeserializableJsonRequestObjectInterface
{

}
