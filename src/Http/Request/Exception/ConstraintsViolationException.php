<?php

declare(strict_types=1);

namespace App\Http\Request\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

use function sprintf;

class ConstraintsViolationException extends HttpException
{
    /**
     * @var ConstraintViolationListInterface<ConstraintViolationInterface>
     */
    private $violations;

    /**
     * @param ConstraintViolationListInterface<ConstraintViolationInterface> $violations
     * @param string    $message
     * @param int       $code
     * @param Throwable $previous
     */
    public function __construct(
        ConstraintViolationListInterface $violations,
        string $message,
        int $code,
        Throwable $previous = null
    ) {
        parent::__construct($code, $this->format($violations, $message), $previous);

        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationListInterface<ConstraintViolationInterface>
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }

    private function format(ConstraintViolationListInterface $violations, string $message): string
    {
        return sprintf("%s\n\n%s", $message, (string)$violations);
    }
}
