<?php declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class AllowExtraFields extends Constraint
{
    const NOT_NULL_ERROR = '4209c46c-cc2e-43f7-bb21-6764da990c78';

    protected static $errorNames = [
        self::NOT_NULL_ERROR => 'ALLOW_EXTRA_FIELDS_ERROR',
    ];

    public $message = 'Extra fields not allowed.';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }
}
