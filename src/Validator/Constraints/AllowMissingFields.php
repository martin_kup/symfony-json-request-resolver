<?php declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class AllowMissingFields extends Constraint
{
    const NOT_NULL_ERROR = '9a9d4fdc-9b2f-493d-aff1-a598aba38eb5';

    protected static $errorNames = [
        self::NOT_NULL_ERROR => 'ALLOW_MISSING_FIELDS_ERROR',
    ];

    public $message = 'Some fields are missing.';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }
}
