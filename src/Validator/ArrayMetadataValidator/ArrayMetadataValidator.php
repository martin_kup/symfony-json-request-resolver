<?php declare(strict_types=1);

namespace App\Validator\ArrayMetadataValidator;

use App\Validator\Constraints\AllowExtraFields;
use App\Validator\Constraints\AllowMissingFields;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UnexpectedValueException;

class ArrayMetadataValidator
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate(array $data, string $type): ConstraintViolationListInterface
    {
        $metadata = $this->validator->getMetadataFor($type);

        if (!$metadata instanceof ClassMetadata) {
            throw new UnexpectedValueException('Unexpected Metadata Class');
        }

        $rootConstraints = $metadata->getConstraints();
        $groupSequence = $metadata->getGroupSequence();

        $allowExtraFields = false;
        $allowMissingFields = false;

        foreach ($rootConstraints as $rootConstraint) {
            if ($rootConstraint instanceof AllowExtraFields) {
                $allowExtraFields = true;

                continue;
            }

            if ($rootConstraint instanceof AllowMissingFields) {
                $allowMissingFields = true;

                continue;
            }
        }

        $propertyConstraints = [];

        foreach ($metadata->getConstrainedProperties() as $property) {
            $propertyMetadataList = $metadata->getPropertyMetadata($property);

            foreach ($propertyMetadataList as $propertyMetadata) {
                $propertyConstraints[$property] = $propertyMetadata->getConstraints();
            }
        }

        $dataConstraints = new Collection(
            [
                'fields'             => $propertyConstraints,
                'allowExtraFields'   => $allowExtraFields,
                'allowMissingFields' => $allowMissingFields,
            ]
        );

        // validate against root (class) constraints
        $violations = $this->validator->validate($data, $rootConstraints);

        // validate against property constraints
        $violations->addAll($this->validator->validate($data, $dataConstraints, $groupSequence));

        return $violations;
    }
}
