<?php

declare(strict_types=1);

namespace App\Resolver\Controller\Argument;

/**
 * Object implementing interface will be hydrated by `DeserializableJsonRequestObjectArgumentResolver`
 * from incoming JSON Request, when used as Controller method argument.
 */
interface DeserializableJsonRequestObjectInterface
{

}
