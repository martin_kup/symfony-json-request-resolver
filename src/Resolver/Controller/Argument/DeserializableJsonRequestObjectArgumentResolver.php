<?php

declare(strict_types=1);

namespace App\Resolver\Controller\Argument;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

use function is_subclass_of;
use function stripos;

/**
 * Purpose:
 *
 *  - Validate object against annotated constraints if object implements `DeserializableJsonRequestObjectInterface`
 *  - Deserialize incoming JSON HTTP Request to object, if object implements `DeserializableJsonRequestObjectInterface`
 *  - On deserialization error throw `BadRequestHttpException`
 *  - On validation error throw `ConstraintsViolationException` handled by `ConstraintsViolationExceptionNormalizer`
 *  - Pass valid object as an argument of controller method for further processing
 *
 * Prerequisites:
 *
 *  - Request HTTP Content-Type is `application/json`
 *  - Argument of Controller method is an object implementing DeserializableJsonRequestObjectInterface interface
 */
class DeserializableJsonRequestObjectArgumentResolver implements ArgumentValueResolverInterface
{
    private const SUPPORTED_CONTENT_TYPE = 'application/json';
    private const SERIALIZER_INPUT_FORMAT = 'json';

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $requestContentType = $request->headers->get('content-type');

        if ($requestContentType === null
            || stripos($requestContentType, self::SUPPORTED_CONTENT_TYPE) !== 0) {
            return false;
        }

        $argumentType = $argument->getType();

        if ($argumentType === null) {
            return false;
        }

        return is_subclass_of($argumentType, DeserializableJsonRequestObjectInterface::class, true);
    }

    /**
     * @inheritDoc
     * @return iterable<object>
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        /* @TODO
         * Aby bylo možné zjistit úplnou property "path" u Constraint Violation,
         * bude třeba předřadit validaci pole před volání Serializeru.
         * Při použití \App\Serializer\Normalizer\ArrayMetadataValidationDenormalizer
         * v rámci Serializeru celou cestu není možné zjistit.(nebo zatím nevím jak, možná půjde použít $context[]).
         */
        try {
            $object = $this->serializer->deserialize(
                $request->getContent(),
                (string)$argument->getType(),
                self::SERIALIZER_INPUT_FORMAT
            );
        } catch (NotEncodableValueException $exception) {
            throw new BadRequestHttpException('Bad Request: Not Encodable Request Body', $exception);
        }

        yield $object;
    }
}
