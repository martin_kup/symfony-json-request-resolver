<?php declare(strict_types=1);

namespace App\Controller;

use App\Http\Response\JsonProblemResponse;
use App\Logger\LoggerTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    use LoggerTrait;

    public function jsonProblem(
        string $title,
        string $detail,
        int $status,
        $data = null,
        ?string $type = null
    ): JsonProblemResponse
    {
        return new JsonProblemResponse($title, $detail, $status, $type, $data);
    }

    public function jsonStatus(int $status): JsonResponse
    {
        return JsonResponse::fromJsonString('{}', $status);
    }
}
