<?php declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Api\V1\Dto\IdDto;
use App\Api\V1\Product\Price\PriceRequestDto;
use App\Api\V1\Product\PriceCreator;
use App\Api\V1\Product\PriceMslCreator;
use App\Api\V1\Validator\Factory\ConstraintViolationListJsonResponseFactory;
use App\Api\V1\Validator\Price\HardwarePriceValidator;
use App\Repository\PriceRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Controller extends ApiController
{
    private $entityManager;
    private $productRepository;
    private $priceRepository;
    private $priceCreator;
    private $priceMslCreator;
    private $hardwarePriceValidator;
    private $constraintViolationListJsonResponseFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        PriceRepository $priceRepository,
        PriceCreator $priceCreator,
        PriceMslCreator $priceMslCreator,
        HardwarePriceValidator $hardwarePriceValidator,
        ConstraintViolationListJsonResponseFactory $constraintViolationListJsonResponseFactory
    ) {
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->priceRepository = $priceRepository;
        $this->priceCreator = $priceCreator;
        $this->priceMslCreator = $priceMslCreator;
        $this->hardwarePriceValidator = $hardwarePriceValidator;
        $this->constraintViolationListJsonResponseFactory = $constraintViolationListJsonResponseFactory;
    }

    /**
     * @Route("/1.0/products/{productId<%uuid.v4.regex%>}/prices", methods="POST")
     */
    public function handle(PriceRequestDto $priceRequestDto, string $productId): Response
    {
        $this->logInfo('Create Price <<<', ['productId' => $productId]);

        $product = $this->productRepository->find($productId);

        if ($product === null) {
            $this->logInfo(
                'Create Price >>> Failed (Product not found)',
                ['productId' => $productId]
            );

            return $this->jsonProblem('Not Found', 'Product not found', Response::HTTP_NOT_FOUND);
        }

        $price = $this->priceRepository->findOneByProductAndValidFrom($productId, $priceRequestDto->getValidFrom());

        if ($price !== null) {
            $this->logInfo(
                'Create Price >>> Failed (Price exists)',
                ['productId' => $productId]
            );

            return $this->jsonProblem('Conflict', 'Price exists', Response::HTTP_CONFLICT);
        }

        $violationList = $this->hardwarePriceValidator->validate($product, $priceRequestDto);

        if ($violationList->count() > 0) {
            return $this->constraintViolationListJsonResponseFactory->create($violationList);
        }

        $price = $this->priceCreator->create($product, $priceRequestDto);

        if ($priceRequestDto->getMslId() !== null) {
            $priceMsl = $this->priceMslCreator->create($price, $priceRequestDto->getMslId());

            $this->entityManager->persist($priceMsl);
        }

        $responseDto = new IdDto($price);

        $this->entityManager->persist($price);
        $this->entityManager->flush();

        $this->logInfo('Create Price >>> Success', ['id' => $price->getId()]);

        return $this->json($responseDto, Response::HTTP_CREATED);
    }
}
