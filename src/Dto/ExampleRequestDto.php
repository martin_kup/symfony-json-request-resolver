<?php declare(strict_types=1);

namespace App\Api\V1\Product\Price;

use DateTime;
use App\Http\Request\RequestObjectDto;
use Symfony\Component\Validator\Constraints as Assert;

class ExampleRequestDto extends RequestObjectDto
{
    /**
     * @var float|null
     * @Assert\Type("float")
     * @Assert\Positive
     */
    private $standardPrice;

    /**
     * @var float|null
     * @Assert\Type("float")
     * @Assert\Positive
     */
    private $standardPromoPrice;

    /**
     * @var string|null
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="255")
     */
    private $standardPromoPriceValidity;

    /**
     * @var float|null
     * @Assert\Type("float")
     * @Assert\Positive
     */
    private $contractPrice;

    /**
     * @var float|null
     * @Assert\Type("float")
     * @Assert\Positive
     */
    private $contractPromoPrice;

    /**
     * @var string|null
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="255")
     */
    private $contractPromoPriceValidity;

    /**
     * @var float
     * @Assert\Type("float")
     * @Assert\PositiveOrZero
     * @Assert\NotNull
     */
    private $depositPrice;

    /**
     * @var DateTime
     * @Assert\DateTime(format="Y-m-d\TH:i:sP")
     * @Assert\NotNull
     */
    private $validFrom;

    /**
     * @var string|null
     * @Assert\Type("string")
     * @Assert\Uuid(strict="true")
     */
    private $mslId;

    /**
     * @var string|null
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="255")
     */
    private $description;

    /**
     * Symfonní Serializer ve výchozím nastavení potřebuje mít Constructor + Gettery.
     *
     * @param float|null  $standardPrice
     * @param float|null  $standardPromoPrice
     * @param string|null $standardPromoPriceValidity
     * @param float|null  $contractPrice
     * @param float|null  $contractPromoPrice
     * @param string|null $contractPromoPriceValidity
     * @param float       $depositPrice
     * @param DateTime    $validFrom
     * @param string|null $mslId
     * @param string|null $description
     */
    public function __construct(
        ?float $standardPrice,
        ?float $standardPromoPrice,
        ?string $standardPromoPriceValidity,
        ?float $contractPrice,
        ?float $contractPromoPrice,
        ?string $contractPromoPriceValidity,
        float $depositPrice,
        DateTime $validFrom,
        ?string $mslId,
        ?string $description
    ) {
        $this->standardPrice = $standardPrice;
        $this->standardPromoPrice = $standardPromoPrice;
        $this->standardPromoPriceValidity = $standardPromoPriceValidity;
        $this->contractPrice = $contractPrice;
        $this->contractPromoPrice = $contractPromoPrice;
        $this->contractPromoPriceValidity = $contractPromoPriceValidity;
        $this->depositPrice = $depositPrice;
        $this->validFrom = $validFrom;
        $this->mslId = $mslId;
        $this->description = $description;
    }

    public function getStandardPrice(): ?float
    {
        return $this->standardPrice;
    }

    public function getStandardPromoPrice(): ?float
    {
        return $this->standardPromoPrice;
    }

    public function getStandardPromoPriceValidity(): ?string
    {
        return $this->standardPromoPriceValidity;
    }

    public function getContractPrice(): ?float
    {
        return $this->contractPrice;
    }

    public function getContractPromoPrice(): ?float
    {
        return $this->contractPromoPrice;
    }

    public function getContractPromoPriceValidity(): ?string
    {
        return $this->contractPromoPriceValidity;
    }

    public function getDepositPrice(): float
    {
        return $this->depositPrice;
    }

    public function getValidFrom(): DateTime
    {
        return $this->validFrom;
    }

    public function getMslId(): ?string
    {
        return $this->mslId;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
