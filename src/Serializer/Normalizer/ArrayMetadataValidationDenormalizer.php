<?php declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Http\Request\Exception\ConstraintsViolationException;
use App\Validator\ArrayMetadataValidator\ArrayMetadataValidator;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use function is_array;

class ArrayMetadataValidationDenormalizer implements ContextAwareDenormalizerInterface
{
    private $normalizer;
    private $validator;

    public function __construct(ObjectNormalizer $normalizer, ArrayMetadataValidator $validator)
    {
        $this->normalizer = $normalizer;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $format === 'json';
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $violations = $this->validator->validate($data, $type);

        if (count($violations) > 0) {
            throw new ConstraintsViolationException($violations, 'Bad Request: Data Validation Failed', 400);
        }

        return $this->normalizer->denormalize($data, $type, $format);
    }
}
