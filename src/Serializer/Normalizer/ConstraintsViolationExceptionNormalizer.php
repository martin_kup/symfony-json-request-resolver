<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use ArrayObject;
use App\Http\Request\Exception\ConstraintsViolationException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use UnexpectedValueException;

use function sprintf;

/**
 * Transforms ConstraintsViolationException to RFC 7807 compatible HTTP Response.
 */
class ConstraintsViolationExceptionNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    /**
     * @var SerializerInterface&NormalizerInterface
     */
    protected $serializer;

    /**
     * @param mixed       $exception
     * @param string|null $format
     * @param mixed[]     $context
     *
     * @return array<mixed>|ArrayObject|bool|float|int|string|null
     * @throws ExceptionInterface
     */
    public function normalize($exception, string $format = null, array $context = [])
    {
        $constraintsViolationException = $context['exception'];

        if (!$constraintsViolationException instanceof ConstraintsViolationException) {
            throw new UnexpectedValueException(
                sprintf('Expected %s exception class', ConstraintsViolationException::class)
            );
        }

        return $this->serializer->normalize($constraintsViolationException->getViolations(), $format, $context);
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FlattenException
            && $data->getClass() === ConstraintsViolationException::class;
    }

    /**
     * @required
     * @param SerializerInterface&NormalizerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }
}
